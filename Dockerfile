FROM nginx:stable
RUN mkdir -p /var/www/html
COPY hello.txt /var/www/html/
RUN cp -rf /usr/share/nginx/html/* /var/www/html/ && rm -rf /usr/share/nginx/html
RUN ln -s /var/www/html /usr/share/nginx

